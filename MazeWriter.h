/***************************************************************************//**
@brief  Contains data structures to write a solution to a maze to file.
@file   MazeWriter.h
@author Jamal Rahhali
@date   February 13, 2013
*******************************************************************************/
#ifndef MAZE_WRITER_H
#define MAZE_WRITER_H

#include <string>
#include <ios>
#include "Maze.h"

class MazeWriter
{
public:
    /**
    @brief Writes a solution to a maze to file.
    @param fileName The name of the solution file.
    @param maze The Maze that has been solved
    @throws std::fstream::failure if error writing the file */
    static void WriteToFile(const std::string& fileName, const Maze& maze);

    /**
    @brief Writes a solution to a maze to the specifed std::ostream
    @param outStream An outstream to write a Maze to
    @param maze A Maze to write to outStream */
    static void WriteToStream(std::ostream& outStream, const Maze& maze);
};

#endif // MAZE_WRITER_H
