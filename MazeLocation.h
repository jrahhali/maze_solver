/***************************************************************************//**
@brief  Contains data structures to for representing a location in a maze
@file   MazeLocation.h
@author Jamal Rahhali
@date   February 13, 2013
*******************************************************************************/
#ifndef MAZE_LOCATION_H
#define MAZE_LOCATION_H

/**
@brief Represents a location in a Maze */
struct MazeLocation
{
    unsigned int X;
    unsigned int Y;

    bool operator==(const MazeLocation& mazeLocation) const
    {
        if(this->X == mazeLocation.X && this->Y == mazeLocation.Y)
        {
            return true;
        }
        return false;
    }
};

#endif // MAZE_LOCATION_H
