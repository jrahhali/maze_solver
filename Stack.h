/***************************************************************************//**
@brief  Contains data structures that represent a Stack
@file   Stack.h
@author Jamal Rahhali
@date   February 10, 2013
*******************************************************************************/
#ifndef STACK_H
#define STACK_H

#include <stdexcept>
#include <string>
#include "Node.h"

class StackException : public std::logic_error
{
public:
    StackException(const std::string& message = "")
        : std::logic_error(message.c_str())
    {
    }
};

/**
@brief  Represents a Stack
@tparam The type of data that this Stack will hold */
template<typename T> class Stack
{
public:
    /**
    @brief Constructs a new Stack */
    Stack() : _topNode(nullptr), _stackSize(0)
    {
    }

    /**
    @brief Pops everything off this Stack */
    ~Stack()
    {
        while(_topNode != nullptr)
        {
            Pop();
        }
    }

    /**
    @brief  Pushes a new data item onto this stack
    @param  data The data to push
    @return A reference to this Stack */
    Stack<T>& Push(const T& data)
    {
        Node<T>* newNode = new Node<T>(data);
        if(_topNode == nullptr)
        {
            _topNode = newNode;
        }
        else
        {
            newNode->SetPreviousNode(_topNode);
            _topNode = newNode;
        }
        _stackSize++;
        return *this;
    }

    /**
    @brief Pops an item off this Stack
    @return A reference to this Stack
    @throws std::out_of_range If stack is empty */
    Stack<T>& Pop()
    {
        if(_topNode == nullptr)
        {
            throw std::out_of_range("Cannot pop - stack empty.");
        }
        Node<T>* tempNode = _topNode->GetPreviousNode();
        delete _topNode;
        _topNode = tempNode;
        _stackSize--;
        return *this;
    }

    /**
    @brief  Gets the current top/head of this Stack
    @return A reference to the data located at the head of this Stack
    @throws StackException if stack is empty */
    const T& GetHead() const
    {
        if(_topNode != nullptr)
        {
            return _topNode->GetData();
        }
        else
        {
            throw StackException("Cannot access head - stack empty.");
        }
    }

    /**
    @brief  Gets the size of this Stack
    @return The size of this Stack */
    unsigned int GetSize() const
    {
        return _stackSize;
    }

private:
    Node<T>* _topNode;
    unsigned int _stackSize;
};
#endif // STACK_H

