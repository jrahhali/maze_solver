/***************************************************************************//**
@brief  Call this function from main() to begin the application
@file   ConsoleMazeApplication.h
@author Jamal Rahhali
@date   February 13, 2013

I wanted to try and use the static keyword for a few of it's different
purposes besides just making a member variable static.  That's why you'll see
static global variables and functions and static local variables.
*******************************************************************************/
#include <iostream>
#include <fstream>
#include <cstdlib>
#include "Maze.h"
#include "MazeSolver.h"
#include "Stack.h"
#include "MazeLocation.h"
#include "MazeWriter.h"

static void printSolutionFrameByFrame();
static Maze maze("maze.txt");
static MazeSolver mazeSolver(maze);

/**
@brief Starts the maze solver application.  All logic for the program is in
this method

a file named maze.txt must be in the same directory as the executable. */
void ConsoleMazeApplication_Invoke()
{
    try
    {
        std::cout << "Solving..." << std::endl;
        while(mazeSolver.Step())
        {
            /* Comment out the line below to solve the maze 'automatically'
            and without visuals */
            printSolutionFrameByFrame()
            ;
        }
        MazeWriter::WriteToFile("solution.txt", mazeSolver.GetMaze());
        std::cout
            << "Complete.  Solution saved to file solution.txt" << std::endl;
    }
    catch(const std::fstream::failure& ex)
    {
        std::cout << "Error: " << ex.what();
    }
}

void printSolutionFrameByFrame()
{
    static bool isStepping = true;
    system("cls");
    MazeWriter::WriteToStream(std::cout, mazeSolver.GetMaze());
    std::cout << std::endl << "[return] key to step.  'f' key + [return] to ";
    std::cout << "fast forward (warning: this is flickery!).";
    if(isStepping && (std::cin.get()) == 'f')
    {
        isStepping = false;
    }
}
