/***************************************************************************//**
@brief  Contains data structures that represent a maze.
@file   Maze.h
@author Jamal Rahhali
@date   February 13, 2013
*******************************************************************************/
#ifndef MAZE_H
#define MAZE_H

#include <string>
#include "MazeLocation.h"

/**
@brief Represents the different tiles of a maze

WALL: a maze wall
GOOD: a tile that has not been explored and is not a WALL
BAD: a tile that has been explored and is not a WALL
SOLUTION: a tile that is part of the maze solution */
enum MazeTiles
{
    WALL,
    GOOD,
    BAD,
    SOLUTION
};

/**
@brief Reprsents a maze */
class Maze
{
public:
    static const unsigned int MAZE_WIDTH = 51;
    static const unsigned int MAZE_HEIGHT = 51;
    static constexpr MazeLocation START_LOCATION{0, 1};
    static constexpr MazeLocation END_LOCATION{50, 49};

    /**
    @brief  Constructs a Maze from the file given
    @param  mazeFileName the file that contains an ASCII maze
    @throws std::fstream::failure if error reading file

    The file that contains the maze must be a 51 x 51 maze (not including
    newline/carriage return characters) made up of ASCII characters.  It must
    not contains any whitespace around the maze (again, besides the newline
    character.  Furthermore, the start position must be at location (0, 1) and
    the end location at (50, 49) */
    Maze(const std::string& mazeFileName);

    /**
    @brief Marks a MazeLocation in this Maze
    @param mazeLocation The MazeLocation to mark
    @param tile the MazeTile to set */
    void SetTileAtLocation(const MazeLocation& mazeLocation, MazeTiles tile);

    /**
    @brief Checks whether a certain MazeLocation contains a MazeTile
    @param mazeLocation A MazeLocation to check
    @param tile A MazeTile
    @returns True if tile is at mazeLocation.  False otherwise */
    bool IsTileAtLocation(const MazeLocation& mazeLocation, MazeTiles tile)
    const;

    /**
    @brief  Returns the ASCII character that represnts thsi maze in the ASCII
    file at a certain MazeLocation.
    @param  mazeLocaton The MazeLocation that contains the char you want
    returned
    @return ASCII character at mazeLocation*/
    char GetCharAt(const MazeLocation& mazeLocation) const;

private:
    static const char _FREESPACE_CHARACTER{' '};
    MazeTiles _maze[MAZE_WIDTH * MAZE_HEIGHT];
    char _asciiMaze[MAZE_WIDTH * MAZE_HEIGHT];
    void _readFileIntoMaze(const std::string& mazeFileName);
};

#endif // MAZE_H
