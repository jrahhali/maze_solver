/***************************************************************************//**
@brief  Entry point of program.  Simply contains int main().
@file   main.cpp
@author Jamal Rahhali
@date   February 13, 2013
*******************************************************************************/
#include "ConsoleMazeApplication.h"

/**
@brief Entry point of program */
int main()
{
    ConsoleMazeApplication_Invoke();
    return 0;
}
