/***************************************************************************//**
@brief  see header
@file   MazeWriter.cpp
@author Jamal Rahhali
@date   February 13, 2013
*******************************************************************************/
#include <string>
#include <fstream>
#include "Maze.h"
#include "MazeWriter.h"

void MazeWriter::WriteToFile(const std::string& fileName, const Maze& maze)
{
    try
    {
        std::ofstream solutionFile;
        solutionFile.exceptions(std::ofstream::failbit | std::ofstream::badbit);
        solutionFile.open(fileName);
        MazeWriter::WriteToStream(solutionFile, maze);
        solutionFile.close();
    }
    catch(const std::fstream::failure& ex)
    {
        throw std::fstream::failure
            ("There was a problem writing to the maze file.");
    }
}

void MazeWriter::WriteToStream(std::ostream& outStream, const Maze& maze)
{
    for(unsigned int row = 0; row < Maze::MAZE_HEIGHT; row++)
    {
        for(unsigned int column = 0; column < Maze::MAZE_WIDTH; column++)
        {
            if(maze.IsTileAtLocation(MazeLocation{column, row}, SOLUTION))
            {
                outStream << "#";
            }
            else
            {
                outStream << maze.GetCharAt(MazeLocation{column, row});
            }
        }
        if(row + 1 < Maze::MAZE_HEIGHT)
        {
            outStream << std::endl;
        }
    }
}
