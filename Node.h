/***************************************************************************//**
@brief  Contains data structures that represent a Node in a Stack
@file   Node.h
@author Jamal Rahhali
@date   February 10, 2013
*******************************************************************************/
#ifndef NODE_H
#define NODE_H

/**
@brief  Represents a Node in a Stack
@tparam The type of data that this Node will hold */
template<typename T> class Node
{
public:
    /**
    @brief Constructs a new Node with data
    @param data The data that this Node will hold */
    Node(const T& data) : _data(data), _previousNode(nullptr)
    {
    }

    /**
    @brief Returns the data that this Node holds */
    T& GetData()
    {
        return _data;
    }

    /**
    @brief Returns a pointer to this Node's previous Node */
    Node* const GetPreviousNode()
    {
        return _previousNode;
    }

    /**
    @brief Sets this Node's previous Node
    @param previousNode A pointer to a Node previous to this Node */
    void SetPreviousNode(Node* const previousNode)
    {
        _previousNode = previousNode;
    }

private:
    T _data;
    Node* _previousNode;
};

#endif // NODE_H

