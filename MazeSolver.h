/***************************************************************************//**
@brief  Contains data structures to solve mazes of type Maze
@file   MazeSolver.h
@author Jamal Rahhali
@date   February 13, 2013
*******************************************************************************/
#ifndef MAZE_SOLVER_H
#define MAZE_SOLVER_H

#include "Maze.h"
#include "MazeLocation.h"
#include "Stack.h"

/**
Represents a maze solver */
class MazeSolver
{
public:
    /**
    @brief Constructs a MazeSolver for the supplied Maze
    @param maze The Maze to solve */
    MazeSolver(const Maze& maze);

    /**
    @brief  Steps through the solution to the maze, one move at a time.
    @return True if solution not found.  False if solution found. */
    bool Step();

    /**
    @brief Gets the current Maze for this MazeSolver
    @return constant reference to a Maze */
    const Maze& GetMaze();

private:
    Maze _maze;
    Stack<MazeLocation> _path;

    void _moveToNextLocation();
};

#endif // MAZE_SOLVER_H
