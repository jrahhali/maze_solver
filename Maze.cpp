/*******************************************************************************
* File:    Maze.cpp
* Author:  Jamal Rahhali
* Date:    February 10, 2013
* Purpose: see header.
*******************************************************************************/
#include <fstream>
#include "Maze.h"

/* I'm not sure why these need to be put in the .cpp file.  I get the following
error if I don't have these declarations here:

obj\Debug\MazeSolver.o||In function `ZN10MazeSolverC2ERK4Maze':|
H:\src\data_structures\maze_solver\MazeSolver.cpp|12|undefined reference to `Maze::START_LOCATION'|
H:\src\data_structures\maze_solver\MazeSolver.cpp|13|undefined reference to `Maze::START_LOCATION'|
obj\Debug\MazeSolver.o||In function `ZN10MazeSolver4StepEv':|
H:\src\data_structures\maze_solver\MazeSolver.cpp|18|undefined reference to `Maze::END_LOCATION'|
||=== Build finished: 3 errors, 0 warnings (0 minutes, 2 seconds) ===| */
constexpr MazeLocation Maze::START_LOCATION;
constexpr MazeLocation Maze::END_LOCATION;
const unsigned int Maze::MAZE_WIDTH;
const unsigned int Maze::MAZE_HEIGHT;

Maze::Maze(const std::string& mazeFileName)
{
    _readFileIntoMaze(mazeFileName);
}

/**
@brief  Reads the specified file and populates the _maze array variable
@throws std::fstream::failure if there was a problem reading the maze file. */
void Maze::_readFileIntoMaze(const std::string& mazeFileName)
{
    try
    {
        std::ifstream mazeFile;
        mazeFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        mazeFile.open(mazeFileName);
        for(unsigned int row = 0; row < MAZE_HEIGHT; row++)
        {
            for(unsigned int column = 0; column < MAZE_WIDTH; column++)
            {
                char mazeTile;
                mazeFile.get(mazeTile);
                if(mazeTile == _FREESPACE_CHARACTER)
                {
                    _maze[row * MAZE_WIDTH + column] = GOOD;
                }
                else
                {
                    _maze[row * MAZE_WIDTH + column] = WALL;
                }
                _asciiMaze[row * MAZE_WIDTH + column] = mazeTile;
            }
            //Discard LF, or CR/LF
            if(mazeFile.peek() && !mazeFile.eof())
            {
                std::string discard;
                std::getline(mazeFile, discard);
            }
        }
        mazeFile.close();
    }
    catch(const std::fstream::failure& ex)
    {
        throw std::fstream::failure("Could not read the maze file.");
    }
}

void Maze::SetTileAtLocation(const MazeLocation& mazeLocation, MazeTiles tile)
{
    unsigned int index = mazeLocation.Y * MAZE_WIDTH + mazeLocation.X;
    _maze[index] = tile;
}

bool Maze::IsTileAtLocation(const MazeLocation& mazeLocation, MazeTiles tile)
const
{
    // If mazeLocation is off the board, we will still count it as EXPLORED
    if( _maze[mazeLocation.Y * MAZE_WIDTH + mazeLocation.X] == tile )
    {
        return true;
    }
    return false;
}

char Maze::GetCharAt(const MazeLocation& mazeLocation) const
{
    return _asciiMaze[mazeLocation.Y * MAZE_WIDTH + mazeLocation.X];
}
