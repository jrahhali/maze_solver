/***************************************************************************//**
@brief  see MazeSolver.h
@file   MazeSolver.cpp
@author Jamal Rahhali
@date   February 13, 2013
*******************************************************************************/
#include "MazeSolver.h"
#include "Maze.h"

MazeSolver::MazeSolver(const Maze& maze) : _maze(maze)
{
    _path.Push(Maze::START_LOCATION);
    _maze.SetTileAtLocation(Maze::START_LOCATION, SOLUTION);
}

bool MazeSolver::Step()
{
    try
    {
        if(_path.GetHead() == Maze::END_LOCATION)
        {
            return false;
        }
    }
    catch(const StackException& noSolution)
    {
        return false;
    }

    _moveToNextLocation();
    return true;
}

void MazeSolver::_moveToNextLocation()
{
    const MazeLocation currentLocation = _path.GetHead();
    MazeLocation left{currentLocation.X - 1, currentLocation.Y};
    MazeLocation above{currentLocation.X, currentLocation.Y - 1};
    MazeLocation right{currentLocation.X + 1, currentLocation.Y};
    MazeLocation below{currentLocation.X, currentLocation.Y + 1};

    if(_maze.IsTileAtLocation(left, GOOD))
    {
        _path.Push(left);
        _maze.SetTileAtLocation(left, SOLUTION);
    }
    else if(_maze.IsTileAtLocation(above, GOOD))
    {
        _path.Push(above);
        _maze.SetTileAtLocation(above, SOLUTION);
    }
    else if(_maze.IsTileAtLocation(right, GOOD))
    {
        _path.Push(right);
        _maze.SetTileAtLocation(right, SOLUTION);
    }
    else if(_maze.IsTileAtLocation(below, GOOD))
    {
        _path.Push(below);
        _maze.SetTileAtLocation(below, SOLUTION);
    }
    else
    {
        _maze.SetTileAtLocation(currentLocation, BAD);
        _path.Pop();
    }
}

const Maze& MazeSolver::GetMaze()
{
    return _maze;
}
